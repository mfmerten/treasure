'''
Treasure

A simple text adventure I created while learning Python Programming.

Copyright (c) 2017 Michael Merten <m.merten@protonmail.com>

This software is released under the GNU General Public License version 3.
A copy of the license can be found at:

https://www.gnu.org/licenses/gpl-3.0.en.html

@package treasure

This is the strings module for Treasure.
'''

import sys

def say(message):
    # skip message values of False
    if message is not False:
        print(message)

common = {
    'complaint': "I don't understand you!",
    'forest': "You see an impenetrable forest.",
    'wall': "You see a rock wall.",
    'door': "You see a locked door.",
    'open_door': "You see an open door.",
    'passage': "You see a passage.",
    'door_no_key': "The door is locked. It won't budge.",
    'door_key': "You unlock the door and pass through.",
    'dark': "The room is totally dark. Maybe a torch would help.",
    'go_forest': "The forest is impenetrable. You are forced to turn back.",
    'go_wall': "You run into a wall. ouch!",
    'go_secret': "You pass through the wall like it wasn't even there!",
}

dead = {
    'starved': "You have starved to death!  Way to go dude!",
    'ogre': "You have been slain by the ogre! Bummers.",
    'rat': "You were eaten by the giant rat!",
    'bat': "The bat sucked out all your blood, dude!",
    'sphinx': "You die! Maybe you should practice your riddles?",
}

hunger = {
    'fainting': "You are fainting from hunger... eat soon!",
    'starving': "Your stomach is growling, better eat something.",
    'hungry': "You are hungry, eat some food.",
    'ate': "Ummm, that was good!.",
    'full': "You are full and do not need food.",
    'no_food': "You have no food! Find some!",
}

room_desc = [
    '''You are standing in a forest. To the north is a cave entrance.
    To the south, east and west is an impenetrable forest.''',
    '''You are standing in a cave. To the north is a passage.
    To the south is the cave exit. To the east and west are rock
    walls.''',
    '''You are standing in a cave. To the north is a rock wall. To the
    south, east and west are passages.''',
    '''You are standing in a cave. To the north, south and east are
    passages. To the west is a rock wall.''',
    '''You are standing in a cave. To the north is a passage. To the
    south, east and west are rock walls.''',
    '''You are standing in a cave. To the north and south are passages.
    To the east and west are rock walls.''',
    '''You are standing in a cave. To the north, east and west are rock
    walls. To the south is a passage.''',
    '''You are standing in a cave. To the north you see a locked door.
    It appears to have a bronze lock. To the south and west you see
    passages. To the east is a rock wall.''',
    '''You are standing in a cave. To the north is a passage. To the
    south, east and west are rock walls.''',
    '''You are standing in a cave. To the north and east are rock walls.
    To the south is an open door. To the west is a passage.''',
    '''You are standing in a cave. To the north is a locked door. It
    appears to have a silver lock. To the south and west are rock walls.
    To the east is a passage.''',
    '''You are standing in a cave. To the north is a locked door. To
    the south is an open door. To the east is a passage. To the west
    is a rock wall.''',
    '''You are standing in a grotto. To the north and south are rock
    walls. To the east is a glistening pool of water. To the west is
    a passage.''',
    '''You are standing in a cave. To the north and east are rock walls.
    To the south is an open door. To the west is a passage. You notice
    a slight breeze from the east.''',
    '''You are standing in a cave. To the north, south and west are rock
    walls. To the east is a passage.''',
    '''You are standing in a treasure room filled with gold. To the north,
    south, east and west are rock walls.  You notice a slight breeze from
    the west.''',
    '''You are standing in a pool of water. To the north, south and east
    are rock walls. To the west you can see dry land.''',
]

go_west = [
    common['go_forest'],
    False,
    common['go_wall'],
    common['go_wall'],
    common['go_wall'],
    common['go_wall'],
    False,
    common['go_wall'],
    False,
    common['go_wall'],
    common['go_wall'],
    False,
    False,
    common['go_wall'],
    common['go_secret'],
    False,
 ]

go_east = [
    common['go_forest'],
    common['go_wall'],
    False,
    False,
    common['go_wall'],
    common['go_wall'],
    common['go_wall'],
    common['go_wall'],
    common['go_wall'],
    common['go_wall'],
    False,
    False,
    "You wade out into the water. Its cold!",
    common['go_secret'],
    False,
    common['go_wall'],
    common['go_wall'],
]

go_north = [
    "You enter the cave.",
    False,
    common['go_wall'],
    False,
    False,
    False,
    common['go_wall'],
    False,
    False,
    common['go_wall'],
    False,
    False,
    common['go_wall'],
    common['go_wall'],
    common['go_wall'],
    common['go_wall'],
    common['go_wall'],
]

go_south = [
    common['go_forest'],
    "You leave the cave.",
    False,
    False,
    common['go_wall'],
    False,
    False,
    False,
    common['go_wall'],
    False,
    common['go_wall'],
    False,
    common['go_wall'],
    False,
    common['go_wall'],
    common['go_wall'],
    common['go_wall'],
]

exit_north = [1,2,False,5,3,6,False,9,7,False,11,13,False,False,False,False,
              False]

exit_south = [False,0,1,4,False,3,5,8,False,7,False,10,False,11,False,False,
              False]

exit_east = [False,False,7,2,False,False,False,False,False,False,9,12,16,
             15,13,False,False]

exit_west = [False,False,3,False,False,False,False,2,False,10,False,False,
             11,14,False,13,12]

room_lit = [
    True,
    True,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
]

room_item = [
    'food',
    'torch',
    'food',
    'key1',
    'food',
    False,
    'key3',
    False,
    'sword',
    False,
    False,
    'food',
    False,
    False,
    False,
    'gold',
    'armor',
]

item_desc = {
    'food': 'There is some food here.',
    'key1': 'There is a bronze key here.',
    'key2': 'There is a silver key here.',
    'key3': 'There is a gold key here.',
    'torch': 'There is a flaming torch here.',
    'sword': 'There is a shiny sword here.',
    'helmet': 'There is a sturdy helmet here.',
    'armor': 'There is a glistening suit of armor here.',
    'gold': 'There is a heaping pile of gold here.',
}
    

take = {
    'food': "You pick up the food.",
    'torch': "You pick up the torch.",
    'key1': "You pick up the bronze key.",
    'key2': "You pick up the silver key.",
    'key3': "You pick up the gold key.",
    'sword': "You pick up the sword.",
    'helmet': "You pick up the helmet and put it on.",
    'armor': "You pick up the armor and put it on.",
    'gold': "You got the gold! Yay!  You Win!",
}

critter = [
    False,
    False,
    False,
    False,
    False,
    "a huge ogre",
    False,
    False,
    False,
    "a giant rat",
    False,
    False,
    False,
    "a giant bat",
    "the great sphinx",
    False,
    False,
]

menu = {
    'h': "h(elp)",
    'n': "n(orth)",
    's': "s(outh)",
    'e': "e(ast)",
    'w': "w(est)",
    'd': "d(own)",
    'g': "g(o)",
    'l': "l(ook)",
    'f': "f(ood)",
    'a': "a(ttack)",
    'r': "r(un)",
    't': "t(ake)",
    'q': "q(uit)",
}

restrict = {
    7: {'n': 'key1'},
    10: {'n': 'key2'},
    11: {'n': 'key3'},
}

riddles = [
    "What do you call a cow with no legs?",
    "What do you call a fly with no wings?",
    "What is black and white and red all over?",
]

answers = [
    "ground beef",
    "a walk",
    "a skunk with diaper rash",
]


