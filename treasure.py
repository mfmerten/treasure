#!/usr/bin/env python
'''
Treasure

A simple text adventure I created while learning Python Programming.

Copyright (c) 2017 Michael Merten <m.merten@protonmail.com>

This software is released under the GNU General Public License version 3.
A copy of the license can be found at:

https://www.gnu.org/licenses/gpl-3.0.en.html

@package treasure
'''
import sys

from _strings import *

global player

VERSION = "0.1.0"

#--- Helper Classes -----------------------------#

### Room Class

class Room(object):
    room = 0
    
    def commands(self):
        commands = ['h','n','s','e','w','q','l','f']
        
        if room_item[self.room]:
            commands.append('t')
        
        if critter[self.room]:
            commands.append('a')
        
        return sorted(commands)
    
    def look_around(self, has_torch, inventory):
        if room_lit[self.room] or has_torch:
            say(room_desc[self.room])
            if room_item[self.room]:
                if (room_item[self.room] not in inventory or room_item[self.room] == 'food'):
                    say(item_desc[room_item[self.room]])
        else:
            say(common['dark'])
    
    def collide(self, direction):
        # assume direction is valid
        if direction == 'n':
            res = go_north[self.room]
        elif direction == 's':
            res = go_south[self.room]
        elif direction == 'e':
            res = go_east[self.room]
        else:
            res = go_west[self.room]
        
        if res is not False:
            say(res)
    
    def get_exit(self, direction):
        if direction not in ['n','s','e','w']:
            return False
        if direction == 'n':
            return exit_north[self.room]
        elif direction == 's':
            return exit_south[self.room]
        elif direction == 'e':
            return exit_east[self.room]
        else:
            return exit_west[self.room]
    
    def move_player(self, player, direction):
        new_room = self.get_exit(direction)
        if new_room is False:
            self.collide(direction)
            player.move()
            return False
        # from here on, new room is good
        # first, set new room number
        self.room = new_room
        self.look_around(player.has('torch'),player.inventory)
        # do player update
        player.move()

### Character class

class Character(object):
    def __init__(self):
        self.hunger = 0
        self.food = 0
        self.inventory = []
    
    def hungry(self):
        state = ''
        if self.hunger > 20:
            say(dead['starved'])
            return -1
        elif self.hunger > 15:
            state = 'fainting'
        elif self.hunger > 10:
            state = 'starving'
        elif self.hunger > 5:
            state = 'hungry'
        else:
            state = 'sated'
        
        if state != 'sated':
            say(hunger[state])
        return 0
    
    def move(self):
        self.hunger += 1
        if self.hungry() == -1:
            return False
        else:
            return True
    
    def eat_food(self):
        if self.food > 0:
            self.hunger -= 10
            say(hunger['ate'])
            self.food -= 1
        else:
            say(hunger['no_food'])
    
    def take_item(self, item):
        if item == 'food':
            self.food += 1
            say(take['food'])
        else:
            self.inventory.append(item)
            say(take[item])
    
    def has(self, item):
        return (item in self.inventory)

#--- Start Game Loop --------------------------------#

# show game header
print("\nWelcome to the Treasure Game!")
print("We hope you enjoy your visit and that you don't die too soon.\n")

# initialize the character
cave = Room()
cave.room = 0
player = Character()
cave.look_around(player.has('torch'), player.inventory)
player.move()

# start game loop
while True:
    # build a menu for the room
    choices = []
    commands = cave.commands()
    for c in commands: choices.append(menu[c])
    print("\nYour choices are: %s" % ' '.join(choices))
    
    resp = raw_input("What now? (" + ''.join(commands) + ")?>")
    if resp not in commands:
        print("I do not understand. Try h for help.")
        continue
    elif resp == 'q':
        print("OK, I quit.")
        break
    elif resp == 'l':
        cave.look_around(player.has('torch'), player.inventory)
    elif resp in ['n','s','e','w']:
        cave.move_player(player, resp)
    elif resp == 'f':
        player.eat_food()
    elif resp == 't':
        player.take_item(room_item[cave.room])
    else:
        print("Not Implemented Yet!")

# End Game
print("\nGame Over!")
exit(0)

